const phantomjs = require('phantomjs-prebuilt');
const webdriverio = require('webdriverio');
const clc = require("cli-color");
const {CONFIG} = require('./config');
const makeBrowser = (url) => {
    var wdOpts = { desiredCapabilities: { browserName: 'phantomjs' } };
    return webdriverio.remote(wdOpts).init().url(url);
}

const handleSuccess = async (data) => {
    return data;
}

const handleError = err => {
    console.log(err);
}

/**
 * set input value of selected input
 * @param {*} browser 
 * @param {*} selector 
 */
const setInputValueBySelector = async (browser, selector, value) => {
    let result = await browser.setValue(selector, value).then(data => {
        return data;
    }, handleError);
    return result;
}
/**
 * Get input value of selected element
 * @param {*} browser 
 * @param {*} selector 
 */
const getInputValueBySelector = async (browser, selector) => {
    let result = await browser.getValue(selector).then(data => {
        return data;
    }, handleError);
    return result;
}



async function checkLoginHaravan(email, password) {
    let result = new Promise((resolve, reject) => {
        /**
     * Get email and password fill to login form haravan
     */
        if (email && password) {
            console.log(`Openning webpage haravan at ${CONFIG.LOGIN_URL}...`);
            phantomjs.run('--webdriver=4444').then(async program => {
                let selector = CONFIG.EMAIL_INPUT, response, browser = makeBrowser(CONFIG.LOGIN_URL);
                response = await browser.waitForExist(selector, CONFIG.TIMEOUT_INPUT_FIELD).then(handleSuccess, handleError);
                if (response) {
                    console.log(clc.blue('Found email input. Set value email'));
                    response = await setInputValueBySelector(browser, selector, email);
                    console.log(clc.blue('Value email set is'), clc.yellow(await getInputValueBySelector(browser, selector)));
                    selector = CONFIG.PASSWORD_INPUT;
                    response = await browser.waitForExist(selector, CONFIG.TIMEOUT_INPUT_FIELD).then(handleSuccess, handleError);
                    if (response) {
                        console.log(clc.blue('Found password input. Set value password'));
                        response = await setInputValueBySelector(browser, selector, password);
                        console.log(clc.blue('Value password setted is'), clc.yellow(await getInputValueBySelector(browser, selector)));
                        browser.submitForm(CONFIG.FORM_LOGIN).then(res => {
                            //DO NOTHING HERE
                        }, handleError);
                        browser.waitForExist(CONFIG.CONTENT_IDENTIFY_SUCCESS, CONFIG.TIMEOUT_CHECK_LOGIN).then(res => {
                            //TODO HANDLE LOGIC FOR LOGIN SUCCESS
                            console.log(clc.green('Loggin success'));
                            resolve({success: true})
                        }, function () {
                            //TODO HANDLE LOGIC FOR LOGIN ERROR
                            console.log(clc.red('Loggin error'));
                            resolve({success: false, error: 'Invalid credential'})
                        });
                    } else {
                        resolve({success: false, error: 'Incorrect id password input field. Please correct it in selector'})
                    }
                } else {
                    resolve({success: false, error: 'Incorrect id email input field. Please correct it in selector'})
                }
            });
        }
    })
    return result;
}

exports.checkLoginHaravan = checkLoginHaravan; 
