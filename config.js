exports.CONFIG = {
    LOGIN_URL: "https://ibeautyshop.com/account/login",
    EMAIL_INPUT: "#customer_email",
    PASSWORD_INPUT: "#customer_password",
    FORM_LOGIN: "#customer_login",
    CONTENT_IDENTIFY_SUCCESS: ".account-content-w",
    TIMEOUT_INPUT_FIELD: 20000,
    TIMEOUT_CHECK_LOGIN: 20000
}