var express = require('express');
const checkLogin = require('../checkLoginHaravan');
var router = express.Router();

/* GET users listing. */
router.get('/', async (req, res, next) => {
  try {
    let {email, password} = req.query;
    let data = await checkLogin.checkLoginHaravan(email, password);
    res.json(data);
  } catch (e) {
    //this will eventually be handled by your error handling middleware
    next(e) 
  }
});

module.exports = router;
